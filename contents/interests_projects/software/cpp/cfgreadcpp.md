# cfgreadcpp
Config reading class, header-only. Reads key-value pair settings from text config files.

repo: <https://gitlab.com/konradp/cfgreadcpp>

## Example usage
```
#include <iostream>
#include <stdexcept>
#include "cfgreadcpp.hpp"

int main()
{
  CfgRead c;
  c.SetPath("./config.cfg");
  try {
    std::cout << c.GetValue("key2") << std::endl;
  } catch(const std::exception& e) {
    std::cerr << "Failed reading config: " << std::endl
    << e.what() << std::endl;
  }
}
```
