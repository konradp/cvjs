# music

I play left-handed guitar (or upside-down right-handed guitar) and make electronic music.

- [spotify](https://open.spotify.com/artist/0CgtPBymIhhIynoyTGtWkF)
- [nickyfow.bandcamp.com](https://nickyfow.bandcamp.com) (old music albums)
- old guitar videos (2009)  
  [youtube.com/@wizard966/videos](https://www.youtube.com/@wizard966/videos)

TODO: Some inline audio/youtube recordings here

## Software
Music software I'm familiar with:
- **Hydrogen: drum machine**
- **Ardour: DAW**
- Rosegarden: I used to use it as a simple DAW
- MuseScore: typeset music scores
- seq24: I like this sequencer a lot

## My gear
instruments:
- **guitar:**
    - Fender Player stratocaster LH (white)
    - Harrier strat LH (pink)
    - acoustic: Martinez Babe Traveller
- piano: Yamaha CTK-3500
- trumpet: Ruddy Muck
- flute
- alto saxophone
- harmonica

other:
- mixer: Behringer XENYX QX1002USB
- sequencer: Korg SQ-64
- drum machine: Alesis HR-16:B
- monosynth: Behringer CRAVE
- MIDI controller: Arturia MINILAB mkII
- amp sim: nux amp force
- audio interface: Behringer U-PHORIA UMC202HD

dream gear:
- circlon
- multitrack USB mixer
- Behringer pro-800, or some other nice polysynth
- Yamaha PSS-780E (I used to have it)
- piano: Donner DEP-20? Kawai?
