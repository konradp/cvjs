# guitar-chords
A javascript library to draw guitar chords.

repo: <https://gitlab.com/konradp/guitar-chords>  
url: <https://konradp.gitlab.io/guitar-chords/>

<center>
<img style='border: 1px solid' src='media/chords-guitar.png' />
</center>
