if (typeof synth === 'undefined') {
  let synth = new Tinysynth();
  let notes = {
    'C4': 261.63,
    'D4': 293.66,
    'E4': 329.63,
    'F4': 349.23,
    'G4': 392.00,
    'A4': 440.00,
    'B4': 493.88,
    'C5': 523.25,
  };

  const div = document.querySelector('#example');
  for (let note in notes) {
    let btn = document.createElement('button');
    btn.innerHTML = note;
    btn.freq = notes[note];
    div.appendChild(btn);
    btn.addEventListener('mousedown', (e) => {
      synth.noteOn(e.target.freq, duration=250); // 1/4 second
    });
  }
}
