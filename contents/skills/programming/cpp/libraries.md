# libraries

A list of libraries I've used, and links to my projects that use them.


library | purpose | projects
:--- |:--- |:---
**asio**      | network programming, timers | [snippets/timers](https://gitlab.com/konradp/snippets/-/tree/master/cpp/basic/timers)
**cairomm**   | 2D vector graphics, drawing | [tinyplot](https://gitlab.com/konradp/tinyplot)<br>[snippets/cairo](https://gitlab.com/konradp/snippets/-/tree/master/cpp/cairo)
**curl**      | URL transfer | [tinynet](https://gitlab.com/konradp/tinynet)
**glew**      | OpenGL Extension Wrangler | [snippets/3d](https://gitlab.com/konradp/snippets/-/tree/master/cpp/3d)
**glut**      | OpenGL Utility Toolkit | [snippets/3d](https://gitlab.com/konradp/snippets/-/tree/master/cpp/3d)
**gtkmm** | GTK+ graphical user interface | [tinyplot](https://gitlab.com/konradp/tinyplot)<br>[snippets/cairo/gtk](https://gitlab.com/konradp/snippets/-/tree/master/cpp/cairo/gtk)
**jsoncpp**   | reading/writing JSON | [tpm](https://gitlab.com/konradp/tpm)<br>[ssh-mgr](https://gitlab.com/konradp/ssh-mgr)
**liblo**     | lightweight OSC library | [loop-control](https://gitlab.com/konradp/loop-control)
**libuv**     | asynchronous event notification | [loop-control](https://gitlab.com/konradp/loop-control)
**ncurses**   | text-based user interfaces | [ssh-mgr](https://gitlab.com/konradp/ssh-mgr)
**portaudio** | portable audio I/O | [sampler](https://gitlab.com/konradp/sampler)
**rtmidi**    | real-time MIDI I/O | [loop-control](https://gitlab.com/konradp/loop-control)<br>[snippets/audio/rtmidi](https://gitlab.com/konradp/snippets/-/tree/master/cpp/audio/rtmidi)
**sdl**       | Simple DirectMedia Layer | [snippets/3d](https://gitlab.com/konradp/snippets/-/tree/master/cpp/3d)
**sndfile**   | reading/writing audio files | [sampler](https://gitlab.com/konradp/sampler)
