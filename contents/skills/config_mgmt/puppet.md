# puppet

I worked with a puppet repo responsible for config mgmt on a few thousand servers (physical servers + VMs + containers).

I can:

- use right modules for the job, instead of doing everything in bash/sed/awk
- write custom facts
- use templates
- use Hiera
- use notify/require
- troubleshoot and fix failed puppet runs
