# Finance and trading

I'm interested in technical stock trading, and I'm exploring ways in which I can use software to inform my trading decisions.

Hihlighted posts from my [blog-fin](https://konradp.gitlab.io/blog-fin/) blog:

- [Auto generate a Python client/SDK for IB API](https://konradp.gitlab.io/blog-fin/posts/tech-ib-web-api-client-swagger)
- [Fetching NASDAQ stock symbols (python)](https://konradp.gitlab.io/blog-fin/posts/other-fetch-nasdaq-stocks/)
- [Interactive Brokers Client API beta](https://konradp.gitlab.io/blog-fin/posts/tech-ib-api-client-beta/)
- [Interactive Brokers (IB) native Python API (TWS API)](https://konradp.gitlab.io/blog-fin/posts/tech-ib-api-python/)
- [London Stock Exchange study](https://konradp.gitlab.io/blog-fin/posts/study-lse-study/)
- [Trading Friday losers on Monday](https://konradp.gitlab.io/blog-fin/posts/str-trading-friday-losers-on-monday/)

podcasts I listen to:

- Chat with traders
- The Peter Schiff podcast

## fintools-ib
Financial dashboards and ideas which Use Interactive Brokers Client API.

repo: <https://gitlab.com/konradp/fintools-ib>
## finplotjs
Small javascript library for drawing financial candlestick charts.

repo: <https://gitlab.com/konradp/finplotjs>  
project page: <https://konradp.gitlab.io/finplotjs/>

<img class='img80' src='media/finplotjs.png'/>
