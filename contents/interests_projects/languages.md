# Languages

## zhtools
Paste Mandarin text and hover over the characters to show the translation. Choose characters for further study, practice caligraphy, and with flashcards. (language: javascript)

repo: <https://konradp.gitlab.io/zhtools>  
url: <https://konradp.gitlab.io/zhtools/>

### Screenshots
Highlight translated text.

<center><img style='width:80%; border:1px solid' src='media/zhtools.png'/></center>

Pick characters to study.

<center><img style='width:80%; border:1px solid' src='media/zhtools2.png'/></center>

Animate characters to practice stroke order.

<center><img style='width:80%; border:1px solid' src='media/zhtools3.png'/></center>

Practice characters with flashcards.

<center><img style='width:80%; border:1px solid' src='media/zhtools4.png'/></center>
