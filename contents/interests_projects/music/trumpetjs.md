# trumpetjs
Interactive trumpet fingering chart.

demo: <https://konradp.gitlab.io/trumpetjs>  
code: <https://gitlab.com/konradp/trumpetjs>

<img class='img80' src='media/trumpetjs.png' />
