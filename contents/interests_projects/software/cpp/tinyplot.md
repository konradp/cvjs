# tinyplot
A small plotting library for financial timeseries data, header-only.

Uses gtkmm-3.0, cairomm, and tinydate

repo: <https://gitlab.com/konradp/tinyplot>

<center>
<img style='width:80%' src='media/tinyplot.png'/>
</center>
