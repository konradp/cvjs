# drum
A drum sequencer in javascript. See [demo](https://konradp.gitlab.io/drum)

<center>
<img class='img80' src='media/drum.png' />
</center>
