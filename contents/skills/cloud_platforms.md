# Cloud Platforms

I have worked for an UK Cloud computing provider [ElasticHosts](https://en.wikipedia.org/wiki/ElasticHosts). I am also familiar with Amazon AWS.
# aws

I'm currently studying for an AWS Cloud Architect exam. The services I am familiar with are:

- EC2
- S3
- IAM
- CodePipeline, CodeCommit
- more to come

See also my two blog posts:

- [AWS CLI cheatsheet](https://konradp.gitlab.io/blog/post/com-cheatsheet-aws-cli/)
- [AWS DevOps exam course notes](https://konradp.gitlab.io/blog/post/com-aws-devops-training/)
