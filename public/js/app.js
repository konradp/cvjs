let structure = null;
const d = document;
let first = true;
let content = null;
let debug = true;
let page_highlighted = null;
let is_mobile = false;
let is_mobile_menu_shown = false;
window.addEventListener('load', OnLoad());
window.addEventListener('resize', checkMobile);

function OnLoad() {
  //alert(navigator.userAgent);
  fetch('layout.json')
    .then((response => response.json()))
    .then(data => {
      structure = data;
      Draw();
      // TODO: This is hardcoded for now
      fetch('page/about.html')
        .then((response => response.text()))
        .then(data => {
          let page = _getId('page');
          HighlightPage(_getId('page/about.html'));
          page.innerHTML = data;
          hljs.highlightAll();
        })
    })
  checkMobile();
  setStyles();
  const btn_menu = document.querySelector('#btn-menu');
  btn_menu.addEventListener('mousedown', openCloseMenu);
};

function openCloseMenu() {
    // Menu button pressed
    const menu = document.querySelector('#menu');
    const page = document.querySelector('#page');
    is_mobile_menu_shown = !is_mobile_menu_shown;
    if (is_mobile_menu_shown) {
      // show menu, hide page
      menu.className = 'menu-mobile';
      page.style.display = 'none';
    } else {
      // hide menu, show page
      menu.className = 'menu-mobile hidden';
      page.style.display = 'block';
    }

}

function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}

function HighlightPage(el) {
  // Highlight the current page in the tree
  // (but first unhighlight the previously selected)
  if (page_highlighted != null) {
    page_highlighted.className = 'menu_item';
  }
  el.className = 'highlighted';
  page_highlighted = el;
}


function FlipVisible(el) {
  let cur = el.style.getPropertyValue('display');
  if (cur == 'block') {
    // hide
    el.style.setProperty('display', 'none');
    el.style.setProperty('height', 0);
  } else if (cur == 'none') {
    // show
    el.style.setProperty('display', 'block');
    el.style.removeProperty('height');
  }
};


function FlipButton(btn) {
  btn.innerHTML = (btn.innerHTML == '+')? '-' : '+';
};


function _mkItem(item) {
  // LI: button + title
  let is_empty = true;
  if (item.hasOwnProperty('children')
    && item.children.length != 0) {
    is_empty = false;
  }

  let li = _new('li');
  let li_title = _new('span');
  li_title.id = item.url;
  if (['interests_projects', 'skills'].includes(item.name)) {
    li_title.className = 'menu_item main_section';
  } else {
    li_title.className = 'menu_item';
  }
  li_title.innerHTML = item.name;
  if (item.type == 'dir') {
    // Directory: include +/- button
    let li_button = _new('span');
    li_button.className = 'button';
    li_button.innerHTML = (is_empty)? '-' : '+';
    li.appendChild(li_button);
    function expandItem(e) {
      // Fold/unfold on click
      e.stopPropagation(); // Avoid parent onclick
      let t = e.target;
      let child = t.parentNode.querySelector('ul');
      let btn = t.parentNode.querySelector('.button');
      FlipButton(btn);
      FlipVisible(child);
    }
    li_title.addEventListener('click', expandItem);
    li_button.addEventListener('click', expandItem);
  } else {
    // File: load page on the right if clicked
    li_title.src = item.url;
    if (item.scripts) {
      li_title.scripts = item.scripts;
    }
    li_title.addEventListener('click', (e) => {
      if (is_mobile) {
        openCloseMenu();
      }
      HighlightPage(e.target);
      // Load page
      fetch(e.target.src)
        .then((response => response.text()))
        .then(data => {
          let page = _getId('page');
          page.innerHTML = data;
          hljs.highlightAll();
          // Load scripts
          if (e.target.scripts) {
            for (let script of e.target.scripts) {
              fetch(script)
                .then((response => response.text()))
                .then(data => {
                  const el = document.createElement('script');
                  const text = document.createTextNode(data);
                  el.appendChild(text);
                  page.appendChild(el);
                })
            }
          } // if scripts
        }) // fetch page
    }) // click event listener
  }
  li.appendChild(li_title);
  return li;
}


function WalkStructure(root, parent_div, parent_title) {
  let ul = d.createElement('ul');
  ul.className = 'ul-tree';
  //ul.style.setProperty('height', 0);
  if (!first) {
    ul.style.setProperty('display', 'none');
  }
  first = false;
  parent_div.appendChild(ul);
  for (item of root) {
    let li = _mkItem(item);
    ul.appendChild(li);
    if (item.type == 'dir') {
      WalkStructure(item.children, li, item.name);
    }
  }
  if (parent_title == 'interests_projects' || parent_title == 'skills') {
    // TODO: This is hardcoded for now
    FlipVisible(ul);
    let btn = ul.parentNode.querySelector('.button');
    FlipButton(btn);
  }
}

function Draw() {
  let div_menu = document.getElementById('menu');
  WalkStructure(structure, div_menu);
};

function checkMobile() {
  let is_mobile_before = is_mobile;
  if (navigator.userAgent.includes('Mobile')
    || 'ontouchstart' in document.documentElement
  ) {
    // Is mobile
    is_mobile = true;
  } else {
    is_mobile = false;
  }
  if (is_mobile != is_mobile_before) {
    // Update styles
    setStyles();
  }
}


function setStyles() {
  const container = document.querySelector('#container');
  const content = document.querySelector('#content');
  const footer = document.querySelector('#footer');
  const header = document.querySelector('.header');
  const menu = document.querySelector('#menu');
  const page = document.querySelector('#page');
  const btn_menu = document.querySelector('#btn-menu');

  if (is_mobile) {
    //container.style['overflow-y'] = 'hidden'; // TODO: Does this really work on mobile?
    // on PC, it hides the scrollbar, and also can't scroll with mouse
    content.className = 'content-mobile';
    header.style['font-size'] = '4em';
    menu.style = ''; // reset style, hack
    //menu.style['font-size'] = '2em';
    menu.className = 'menu-mobile hidden';
    page.className = 'page-mobile';
    btn_menu.className = 'btn-menu-show';
    footer.className = 'footer footer-mobile';
  } else {
    // PC
    content.className = 'content';
    menu.className = 'menu';
    header.style = ''; // hack
    page.style = ''; // reset style, a hack
    page.className = 'right';
    btn_menu.className = 'btn-menu-hidden';
    footer.className = 'footer';
  }
};
