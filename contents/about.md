# About
<img src='media/konrad.jpg' style='float:right; width:10em; padding: 5px'></img>

I'm a DevOps engineer with a passion for programming and music.
- gitlab: [konradp](https://gitlab.com/konradp)
- linkedin: [kpisarczyk](https://linkedin.com/in/kpisarczyk)
- blog: [konradp.gitlab.io/blog](https://konradp.gitlab.io/blog/post)
- [music: bandcamp](https://nickyfow.bandcamp.com)
- [music: spotify](https://open.spotify.com/artist/0CgtPBymIhhIynoyTGtWkF)

<hr width=70%" style="margin-left:0">

This page is organised into two sections:

**interests (personal)**:
- software/programming
- music: electronic music, guitar music
- Chinese language
- finance and trading

**skills (professional)**:
- programming: **javascript**, **python**, c++, php, bash
- other: linux, **ansible**, **puppet**, git, docker, bash, jenkins

<hr width="70%" style="margin-left:0">

If you like this page, and would like to make your own version, see the **source** link at the bottom-right of this page.
