# html5canvas

I can use the HTML5 Canvas API to draw shapes and text on screen.

## Examples
[finplotjs](https://gitlab.com/konradp/finplotjs): Plot candlestick charts
<img class='img80' src='media/finplotjs.png'></img>


[chords-guitar](https://gitlab.com/konradp/chords-guitar): Draw guitar chords
<img style='width:50%' src='media/chords-guitar2.png'></img>

[trumpetjs](https://gitlab.com/konradp/trumpetjs): Interactive trumpet fingering charts
<img class='img80' src='media/trumpetjs.png'></img>

[drum](https://gitlab.com/konradp/drum): Drum machine/sequencer
<img class='img80' src='media/drum.png'></img>

[snippets/js/libs/html5canvas](https://gitlab.com/konradp/snippets/-/tree/master/js/libs/html5canvas): Some old snippets
