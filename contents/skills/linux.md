# bash
What you can expect:

- readable code:
    - I know when to stop with the **"`|`" pipes**
    - use long params for readability (**`curl -k`** vs **`curl --insecure`**), especially for less-known params
    - clearly named variables
    - clear comments, and where they are appropriate
    - I read the manpages for the tools that I use, e.g. I would write **`ps -o pid,cmd`** instead of **`ps | awk | sed`**-type solutions which can be fragile and hard to read

**Note: In progress**
- [My bash cheatsheet](https://konradp.gitlab.io/blog/post/com-cheatsheet-bash/)
- heredocs - [link](https://gitlab.com/konradp/dotfiles/-/blob/master/config.sh#L9)
- functions with args - [link](https://gitlab.com/konradp/dotfiles/-/blob/master/config.sh#L34)
- ternary operator with `&&` and `||` - [link](https://gitlab.com/konradp/dotfiles/-/blob/master/config.sh#L6)

**Project examples**
- [dotfiles](https://gitlab.com/konradp/dotfiles/-/blob/master/config.sh)


# packaging

I can:
- create RPM packages, example TODO
- TODO: links to projects

blog:
- [cheatsheet: Debian package management](https://konradp.gitlab.io/blog/post/com-debian-pkg-mgmt/)
