# saxjs
Interactive saxophone fingering chart.

demo: <https://konradp.gitlab.io/saxjs>  
code: <https://gitlab.com/konradp/saxjs>

<center>
<img class='img80' src='media/saxjs1.png' />
</center>

Includes alternative fingerings and also allows drawing custom fingerings:
<center>
<img src='media/saxjs2.png' style='border:1px solid; width:30%'/>
</center>
