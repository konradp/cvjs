// This creates:
// - public/page: same structure as the 'contents' dir, but with
//   .html files instead of .md files
// - public/layout.json: tree structure of the page
import {
  existsSync,
  mkdirSync,
  readdirSync,
  readFileSync,
  statSync,
  writeFileSync,
} from 'node:fs';
import showdown from 'showdown';

let tree = [];
const dir_input = 'content';
const dir_output = 'public/page'; 
const converter = new showdown.Converter({tables:true});


function ToHTML(filepath) {
  const text = readFileSync(filepath, { encoding: 'utf8' });
  return converter.makeHtml(text) + '\n';
}


function MkDir(dir) {
  try {
    let s = statSync(dir);
    if (s && s.isDirectory())
      return true;
    else
      console.log('ERROR');
  } catch (error) {
    mkdirSync(dir, { recursive: true });
  }
}


function ToOutputPath(dir) {
  // Replace root dir with dir_output
  dir = dir.split('/');
  dir[0] = dir_output;
  return dir.join('/');
}


function WalkDir(dir, tree) {
  const entries = readdirSync(dir);
  for (const fName of entries) {
    let path = dir + '/' + fName;
    let s = statSync(path);
    let e = {
      name: fName.replace('.md', ''),
      type: null,
    };
    if (s.isDirectory()) {
      e.children = WalkDir(path, []);
      e.type = 'dir';
    } else {
      // Is a file
      e.type = 'file';
      if (fName.endsWith('.md')) {
        // Markdown file
        let dir_out  = ToOutputPath(dir);
        MkDir(dir_out);
        let path_new = dir_out + '/' + fName.replace('.md', '.html');
        const html = ToHTML(path);
        writeFileSync(path_new, html);
        // Remove the 'public' from the url
        path_new = path_new.split('/');
        path_new.shift()
        e.url = path_new.join('/');
        // Scripts
        let fScripts = dir + '/' + fName.replace('.md', '.scripts');
        if (existsSync(fScripts)) {
          console.log('Include scripts:', fScripts);
          let scripts = readFileSync(fScripts, { encoding: 'utf8' }).split('\n');
          scripts.pop();
          e.scripts = scripts;
        }
      } else {
        if (path.endsWith('.scripts')) {
          continue;
        }
        console.log('WARN: Not a markdown file:', path);
        continue;
      }
    }
    tree.push(e);
  }
  tree.sort((a, b) => {
    if (a.type == 'dir' && b.type == 'file') {
      return 1;
    } else if (a.type == b.type) {
      return (a.name > b.name)? 1 : -1;
    } else {
      return -1;
    }
  });
  return tree
}; // WalkDir


// MAIN
tree = WalkDir('contents', tree);
let d = JSON.stringify(tree, null, 2);
writeFileSync('public/layout.json', JSON.stringify(tree, null, 2));
