# cvjs
It's webpage that you are looking at right now. It's a complement of my CV, and lists my skills and interests. (language: node/javascript)

<img src='media/cvjs9.png' class='img80'>

How it works:
- the markdown files are converted to HTML
- the build script walks the directory structure and creates a clickable navigation tree/menu on the left side of the page

## Reference
- [repo: gitlab](https://gitlab.com/konradp/cvjs)
- [blog: project diary](https://konradp.gitlab.io/blog/post/com-cvjs1/)
