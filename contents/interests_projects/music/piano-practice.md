# piano-practice
A collection of interactive piano exercises for intervals, scales etc. Includes flashcards, live MIDI controller exercises, music theory notes.

demo: <https://konradp.gitlab.io/piano-practice>  
code: <https://gitlab.com/konradp/piano-practice>

<center><img style='width:80%; border:1px solid' src='media/piano-practice.png'/></center>
