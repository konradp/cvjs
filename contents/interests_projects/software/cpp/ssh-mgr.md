# ssh-mgr
An ncurses based text mode SSH connection manager for spawning SSH connections (e.g. new xterm windows).

repo: <https://gitlab.com/konradp/ssh-mgr>

<center>
<img src='media/ssh-mgr.png'/>
</center>
