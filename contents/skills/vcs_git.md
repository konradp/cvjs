# git

What I can do and have experience in:

- clear commit messages
- small and clean PRs without scope creep
- being kind and impartial during peer review
- working with branches
- squashing commits when needed for a clean git log history
- resolving merge conflicts
- reading diffs
- working with tags
- solving three-way merges
- pre-commit hooks

## git-related services
I have used these:
- GitLab: I use it also for personal projects, and I worked with:
    - CI/CD pipelines
    - GitLab Pages
    - issue boards
    - PRs (merge requests), auto-closing issues (`Fixes: #123`)
    - GitLab workers
    - Docker image registry
- GitHub:
    - PRs
    - managing organizations
    - managing permissions
    - Github Actions
- Bitbucket: I used it in one of the jobs

## Commit message template
```
Add XYZ

This adds a functionality XYZ, and this message has
lines of length 70 or less.

Signed-off-by: Konrad Pisarczyk <kpisarczyk@gmail.com>
```
ref: [github.com/subsurface](https://github.com/subsurface/subsurface#contributing)


# mercurial
I have done:
- worked with branches, tags
- migrated repos from mercurial to git
- three-way merges
- managed access to mercurial repos


# svn
I have used SVN combined with Phabricator for code review.
