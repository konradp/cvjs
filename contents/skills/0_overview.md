# Skills
My main skills:

<div id='skills'></div>

- linux
- git
- python
- javascript
- bash
- CICD
- Jenkins
- Jenkinsfile
- monitoring: Nagios, Sensu
- docker, docker-compose
- AWS, AWS CLI
- mysql
- php
- c++
- Confluence/JIRA, Phabricator


<script>
console.log('oooo');
alert('test');
const skills = [
  'linux',
  'git',
  'python',
  'javascript',
  'bash',
  'CICD',
  'Jenkins',
  'Jenkinsfile',
  {'monitoring': ['Nagios', 'Sensu']},
  ['docker', 'docker-compose']
  ['AWS', 'AWS CLI'],
  'mysql',
  'php',
  'c++',
  ['Confluence/JIRA', 'Phabricator']
const d = document.getElementById('skills');
skills.innerHTML = 'trololo';
</script>


# Writing style

## Blog articles
These blog articles demonstrate the way I research, troubleshoot, think, and write:

- [COM: vim error: BufWinEnter Autocommands: Unknown option: termmode=](https://konradp.gitlab.io/blog/post/com-vim-bufwinenter-error/): Troubleshooting a vim error after an OS upgrade.
- [Project journal for CVJS web app](https://konradp.gitlab.io/blog/post/com-cvjs1/)
- [javascript: Move canvas elements with the mouse](https://konradp.gitlab.io/blog/post/com-js-canvas-move/)
- [Understanding callbacks in C++](https://konradp.gitlab.io/blog/post/com-cpp-class-member-callbacks/)
- [Example for plotting stock data charts with R](https://konradp.gitlab.io/blog/post/fin-trading-friday-losers-on-monday/)
- https://konradp.gitlab.io/blog/post/com-choosing-hugo-for-blog/

## Cheatsheets

main points:

- use direct sources (reference manuals instead of stackoverflow or blog articles)

cheatsheets:

- https://konradp.gitlab.io/blog/post/com-aws-cli-cheatsheet/
- https://konradp.gitlab.io/blog/post/com-debian-pkg-mgmt/

guides and walkthroughs:

- https://konradp.gitlab.io/blog/post/fin-interactive-brokers-web-api-client-swagger/
- https://konradp.gitlab.io/blog/post/fin-interactive-brokers-api-client-beta/
- https://konradp.gitlab.io/blog/post/fin-interactive-brokers-api-python/
- https://konradp.gitlab.io/blog/post/com-php-api-crime-info/
