# piano-draw
A js library to draw a piano keyboard, chords, notes.

demo: <https://konradp.gitlab.io/piano-draw>  
code: <https://gitlab.com/konradp/piano-draw>

<img src="media/piano-draw.png"></img>
