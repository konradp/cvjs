# tinydate
A small library for manipulating dates, header-only.

repo: <https://gitlab.com/konradp/tinydate>

## Example
```
#include <tinydate.hpp>
using std::cout;

tinydate::date d("2018-02-04");
// or: tinydate::date d(2018,2,4);
cout << d.GetString();
cout << d.GetTimestamp();
cout << d.is_leap();
cout << d.prev_day();
cout << d.next_day();
cout << d.prev_month();
cout << d.next_month();
```
