# tinysynth
A minimal synth in javascript for use in my projects.

demo, documentation: <https://konradp.gitlab.io/tinysynth>  
code: <https://gitlab.com/konradp/tinysynth>

Features:
- play single note, e.g.
- specify note by frequency in Hertz (e.g. 440 = A4)
- by name, e.g. A, A#, A4, A#4, B, C, etc
- specify note duration

<h2>Example</h2>
Press on the buttons.
<center>
<div id='example'></div>
</center>
