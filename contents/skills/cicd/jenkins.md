# Jenkins

I can do:
- create build jobs and pipelines in Jenkins UI
- pipelines in Jenkinsfiles
- manage credentials
- manage Jenkins infra: master+agents
- manage plugins
- tag jobs to run on specific agents
- trigger jobs on commits to git repos
- trigger Ansible playbooks from Jenkins jobs
- parametrised jobs: drop-down values, default values
- environment vars: [ref](https://www.jenkins.io/doc/book/pipeline/jenkinsfile/#using-environment-variables)
- notify in Slack on build success/failure
