# tinynet
A small wrapper class for cURL. Provides only minimal functionality: downloads a HTTP response into a string.

repo: <https://gitlab.com/konradp/tinynet>

## Example
```
#include <iostream>
#include <tinynet.hpp>

int main()
{
  Net t;
  std::string page;
  page = t.Get("http://localhost:5000/");
  std::cout << "Received: " << std::endl
    << page << std::endl;
  return 0;
}
```
