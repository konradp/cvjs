# javascript
I like to use pure js for interactive web apps. A typical app of mine uses **NodeJS/express** in the backend, and HTML+js in the frontend.

**Note:** I am aware of jquery but so far I haven't felt the need to use it in my projects.

## Blog
- [JS cheatsheet](https://konradp.gitlab.io/blog/post/com-cheatsheet-js/)
- [NodeJS cheatsheet](https://konradp.gitlab.io/blog/post/com-cheatsheet-node/)
- [javascript: Move canvas elements with the mouse](https://konradp.gitlab.io/blog/post/com-js-canvas-move/)
- [MERN stack](https://konradp.gitlab.io/blog/post/com-react-and-mui/)
- [WIP: docker-compose: React + NodeJS](https://konradp.gitlab.io/blog/post/com-docker-react/)

## Projects
- [baranek](https://gitlab.com/konradp/baranek)  
  Geographic study of the Mass settings in Poland
- [guitar-chords](https://gitlab.com/konradp/guitar-chords)  
  Draw guitar chords with HTML Canvas API
- [cvjs](https://gitlab.com/konradp/cvjs)  
  My interactive CV, i.e. this CV
- [drum](https://gitlab.com/konradp/drum)  
  Drum machine/sequencer
- [finplotjs](https://gitlab.com/konradp/finplotjs)  
  Draw candlestick charts
- [harmonica-explorer](https://gitlab.com/konradp/harmonica-explorer)  
  Highlight which notes on a harmonica correspond to which scale.
- [piano-practice](https://gitlab.com/konradp/piano-practice/-/tree/master/)  
  WIP: Read and plot MIDI controller note velocity notes (for practicing consistent trills)
- [snippets/js](https://gitlab.com/konradp/snippets/-/tree/master/js)  
  Various snippets
- [trumpetjs](https://gitlab.com/konradp/trumpetjs)  
  Interactive trumpet fingerings chart
- [zhtools](https://gitlab.com/konradp/zhtools)  
  Mandarin text translator, study aid, flashcards
- [zhuyin-practice](https://gitlab.com/konradp/zhuyin-practice)  
  Practice the Zhuyin Mandarin Chinese keyboard input (see [wiki: bopomofo](https://en.wikipedia.org/wiki/Bopomofo))

