# C++
My projects which use C++:

project | desc
:--- |:---
[cfgreadcpp](https://gitlab.com/konradp/cfgreadcpp) | read ini-like config files
[loop-control](https://gitlab.com/konradp/loop-control) | control SooperLooper live audio looping app with a MIDI controller
[sampler](https://gitlab.com/konradp/sampler/-/tree/master/) | sound sampler
[ssh-mgr](https://gitlab.com/konradp/ssh-mgr) | text-based SSH connection manager
[snippets/cpp](https://gitlab.com/konradp/snippets/-/tree/master/cpp) | various snippets
[tinydate](https://gitlab.com/konradp/tinydate) | header-only, date manipulation
[tinyhttpparser](https://gitlab.com/konradp/tinyhttpparser) | parse HTTP requests
[tinynet](https://gitlab.com/konradp/tinynet) | header-only, wrapper for curl
[tinyplot](https://gitlab.com/konradp/tinyplot) | header-only, draw candlestick charts
[tinysocketlib](https://gitlab.com/konradp/tinysocketlib) | wrapper for TCP sockets
[tinywebserver](https://gitlab.com/konradp/tinywebserver) | HTTP web server
[tpm](https://gitlab.com/konradp/tpm) | npm-like manager for c++
