# cvjs
demo: https://konradp.gitlab.io/cvjs  
project journal/blog: https://konradp.gitlab.io/blog/post/com-cvjs1/

## How does it work
- showdown: converts markdown to html
- layout.json: a generated tree of the documents

## Run

```
npm install
npm run build
npm start
```
http://localhost:8080

## How to add scripts
create a .scripts file, e.g. in `contents/music/interests_projects/tinysynth.scripts` with contents:
```
https://konradp.gitlab.io/tinysynth/js/tinysynth.js
/js/tinysynth.js
```

# TODO

- later (maybe):
  - link up tags from projects to skills
  - jump to top widget
  - tooltips

Documents TODO:
- databases/postgresql: cheatsheet
- skills/linux
- programming/general
- programming/javascript/libraries
- programming/cpp: add fintools-old
