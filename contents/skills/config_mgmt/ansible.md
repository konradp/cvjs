# ansible
Things I'm familiar with:

- playbooks, roles
- inventory, host vars, group vars etc
- handlers
- JINJA templates
- writing modules (see examples below)
- dry running playbooks, and limiting with `--limit`
- encrypting secrets with Ansible Vault
- using dedicated Ansible modules instead of doing everything with shell, command, grep, sed, awk

## Examples
Examples of Ansible playbooks from my personal projects:

- [fintools-ib/ansible/](https://gitlab.com/konradp/fintools-ib/-/tree/master/ansible), includes:
    - playbooks, roles
    - JINJA templates for config files
    - handlers for reloading config and restarting services
    - role vars
- [crime-info/ansible/](https://gitlab.com/konradp/crime-info/-/tree/master/ansible):
    - includes a custom Ansible module [here](https://gitlab.com/konradp/crime-info/-/blob/master/ansible/library/eh_instance.py)
- [elastichosts-example/ansible/](https://gitlab.com/konradp/elastichosts-example/-/tree/master/ansible)
- [ansible-tutorial/3/roles/nginx/tasks/main.yml](https://gitlab.com/konradp/ansible-tutorial/-/blob/master/tutorial/3/roles/nginx/tasks/main.yml): a small role
