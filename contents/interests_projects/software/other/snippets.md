# snippets
Snippets in bash, c++, and others.
(C++, TeX, bash, javascript, golang)

repo: <https://gitlab.com/konradp/snippets>

<img class='img' src='media/snippets.png'/>
