# loop-control

I have an Arturia MINILAB mkII MIDI controller, which I use to control the [sooperlooper](https://sonosaurus.com/sooperlooper/) live looping sampler via the OSC interface (liblo library). There were two projects to do the same thing, one in C++, and one in python. I don't remember which one was more recent.

- [repo: loop-control](https://gitlab.com/konradp/loop-control)
- [repo: python-sooperlooper](https://gitlab.com/konradp/python-sooperlooper)
- [blog post](https://konradp.gitlab.io/blog/post/com-python-sooperlooper/)
- language: C++ or python
- libraries used: rtmidi, liblo, libuv
- TODO: youtube demo
