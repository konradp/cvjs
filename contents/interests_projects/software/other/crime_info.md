# crime-info
Show a list of crimes and their outcomes for a given postcode and date. Uses UK Police API and postcodes.io.

- [repo](https://gitlab.com/konradp/crime-info)
- [blog post](https://konradp.gitlab.io/blog/post/com-php-api-crime-info/)
- language: PHP
- tools/tech: Docker, CI/CD, RPM package, Ansible
