# python

blog:
- [Python cheatsheet](https://konradp.gitlab.io/blog/post/com-cheatsheet-python/)
- [swagger-codegen and SwaggerEditor](https://konradp.gitlab.io/blog-fin/posts/tech-ib-web-api-client-swagger/)

projects:
- [snippets/python/](https://gitlab.com/konradp/snippets/-/tree/master/python)
- [fintools-ib](https://gitlab.com/konradp/fintools-ib): Lots here:
    - mysql.connector
    - configparser: read .ini files
    - json parsing
    - classes
    - csv: CSV reading
    - function annotation for types
    - f-strings
    - flask
    - list comprehension
    - finnhub: external SDK
    - urllib
    - `import typing`
    - `from dataclasses import dataclass`
- [pyeh](https://gitlab.com/konradp/pyeh): building Python package, upload to PyPI
- [elastichosts-example](https://gitlab.com/konradp/elastichosts-example/-/tree/master/ansible/library): custom Ansible module
