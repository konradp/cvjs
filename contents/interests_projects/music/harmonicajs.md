# harmonicajs
Highlight which notes on a harmonica correspond to which scale.  
**Note:** Adjustable harmonica tonation.  
language: javascript

url: <https://konradp.gitlab.io/harmonicajs/>  
repo: <https://gitlab.com/konradp/harmonicajs>  

<center>
<img style='border: 1px solid' src='media/harmonica-explorer.png' />
</center>
