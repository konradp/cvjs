# GitLab CI/CD
List of my projects that use the `.gitlab-ci.yml`.

- GitLab Pages, node-based apps:
    - [cvjs](https://gitlab.com/konradp/cvjs/-/blob/master/.gitlab-ci.yml)
    - [drum](https://gitlab.com/konradp/drum/-/blob/master/.gitlab-ci.yml)
    - [finplotjs](https://gitlab.com/konradp/finplotjs/-/blob/master/.gitlab-ci.yml)
    - [fincharts](https://gitlab.com/konradp/fincharts/-/blob/master/.gitlab-ci.yml)
    - [piano-practice](https://gitlab.com/konradp/piano-practice/-/blob/master/.gitlab-ci.yml)
    - [sudoku-solver](https://gitlab.com/konradp/sudoku-solver/-/blob/master/.gitlab-ci.yml)
    - [zhtools](https://gitlab.com/konradp/zhtools/-/blob/master/.gitlab-ci.yml)
- GitLab Pages, other:
    - [baranek](https://gitlab.com/konradp/baranek/-/blob/main/.gitlab-ci.yml): static page
    - [blog](https://gitlab.com/konradp/blog/-/blob/master/.gitlab-ci.yml): hugo blog, static page
    - [chords-guitar](https://gitlab.com/konradp/chords-guitar/-/blob/main/.gitlab-ci.yml): static page
    - [history](https://gitlab.com/konradp/history/-/blob/master/.gitlab-ci.yml): static page
    - [node-express-template](https://gitlab.com/konradp/node-express-template/-/blob/main/.gitlab-ci.yml): static page
    - [zhuyin-practice](https://gitlab.com/konradp/zhuyin-practice/-/blob/main/.gitlab-ci.yml): static page
- other
    - [crime-info](https://gitlab.com/konradp/crime-info/-/blob/master/.gitlab-ci.yml): custom img, run Ansible playbook
    - [elastichosts-example](https://gitlab.com/konradp/elastichosts-example/-/blob/master/.gitlab-ci.yml): custom img, run Ansible playbook
    - [fintools-old](https://gitlab.com/konradp/fintools-old/-/blob/master/.gitlab-ci.yml): custom img, run Ansible playbook
    - [fintools-ib](https://gitlab.com/konradp/fintools-ib/-/blob/master/.gitlab-ci.yml): custom img, run Ansible playbook
    - [gitlab-cicd](https://gitlab.com/konradp/gitlab-cicd/-/blob/master/.gitlab-ci.yml): test pipeline
    - [tinydate](https://gitlab.com/konradp/tinydate/-/blob/master/.gitlab-ci.yml): makefile to gcc compile and run tests
    - [webglplay](https://gitlab.com/konradp/webglplay/-/blob/master/.gitlab-ci.yml): custom img, run Ansible playbook
