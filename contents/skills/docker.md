# docker

What I can do:
- Dockerfile
- docker-compose.yaml

Examples from my projects:

- **[fintools-ib**](https://gitlab.com/konradp/fintools-ib/-/blob/master/docker-compose.yml): a more complex example:
    - **[docker-compose.yml](https://gitlab.com/konradp/fintools-ib/-/blob/master/docker-compose.yml)**
    - [Dockerfile1](https://gitlab.com/konradp/fintools-ib/-/blob/master/api/Dockerfile) (FROM python:alpine)
    - [Dockerfile2](https://gitlab.com/konradp/fintools-ib/-/blob/master/dashboard/Dockerfile) (FROM alpine)
- [**ssh-mgr**](https://gitlab.com/konradp/ssh-mgr)  
  uses docker to build RPM pkgs for different distros:
    - [docker/](https://gitlab.com/konradp/ssh-mgr/-/tree/master/docker)
    - [pkg/rpm/](https://gitlab.com/konradp/ssh-mgr/-/tree/master/pkg/rpm)
- [**snippets/dockerCompose1**](https://gitlab.com/konradp/snippets/-/tree/master/devops/docker/dockerCompose1)
- [**snippets/workshop_docker**](https://gitlab.com/konradp/snippets/-/tree/master/devops/docker/workshop_docker)
- [**snippets/sensu-grafana**](https://gitlab.com/konradp/snippets/-/tree/master/misc/sensu-grafana): various dockerfiles
- [**docker-ubuntu-restbed**](https://gitlab.com/konradp/docker-ubuntu-restbed/-/blob/master/Dockerfile) (FROM ubuntu)
- [**crime-info/docker/docker-compose.yml**](https://gitlab.com/konradp/crime-info/-/blob/master/docker/docker-compose.yml)  
  nginx and php-fpm
- [**docker-alpine-ansible**](https://gitlab.com/konradp/docker-alpine-ansible)  
  A Docker image of Alpine Linux with Ansible for use in GitLab CI/CD pipelines.
- [**ansible-tutorial**](https://gitlab.com/konradp/ansible-tutorial/-/tree/master/)
